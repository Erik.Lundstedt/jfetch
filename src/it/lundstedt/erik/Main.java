// -*- mode: indent-tabs; mode: lsp; mode: java; -*-
package it.lundstedt.erik;
public class Main{
	public static void main(String[] args) {

		Config cfg=new Config();
		String[] artwork=cfg.artwork;

		Draw draw=new Draw(artwork,cfg.values);
		draw.drawArt();
	}

}

 
