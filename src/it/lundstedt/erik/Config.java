package it.lundstedt.erik;

public class Config {

	public static final String ANSI_RESET   = "\u001B[0m";
	public static final String ANSI_BLACK   = "\u001B[30m";
	public static final String ANSI_RED     = "\u001B[31m";
	public static final String ANSI_GREEN   = "\u001B[32m";
	public static final String ANSI_YELLOW  = "\u001B[33m";
	public static final String ANSI_BLUE    = "\u001B[34m";
	public static final String ANSI_PURPLE  = "\u001B[35m";
	public static final String ANSI_CYAN    = "\u001B[36m";
	public static final String ANSI_WHITE   = "\u001B[37m";

	public static final String ANSI_BLACK_BACKGROUND   = "\u001B[40m";
	public static final String ANSI_RED_BACKGROUND     = "\u001B[41m";
	public static final String ANSI_GREEN_BACKGROUND   = "\u001B[42m";
	public static final String ANSI_YELLOW_BACKGROUND  = "\u001B[43m";
	public static final String ANSI_BLUE_BACKGROUND    = "\u001B[44m";
	public static final String ANSI_PURPLE_BACKGROUND  = "\u001B[45m";
	public static final String ANSI_CYAN_BACKGROUND    = "\u001B[46m";
	public static final String ANSI_WHITE_BACKGROUND   = "\u001B[47m";

	public KVPair[] values;

	public String[] artwork = Logos.starTrek;

	public Config() {
		String header = System.getProperty("user.name") + "@lubuntu-PC";// only used for calculations

		String separator = "";// this needs to be initialized

		for (int i = 0; i < header.length(); i++) {
			separator += "-";
		}

		String colours=
			ANSI_RED_BACKGROUND      +"  "+
			ANSI_GREEN_BACKGROUND    +"  "+
			ANSI_BLUE_BACKGROUND     +"  "+
			ANSI_YELLOW_BACKGROUND   +"  "+
			ANSI_PURPLE_BACKGROUND   +"  "+
			ANSI_CYAN_BACKGROUND     +"  "+
			ANSI_BLACK_BACKGROUND    +"  "+
			ANSI_WHITE_BACKGROUND    +"  "+
			ANSI_RESET;


		values = new KVPair[artwork.length];// 14 for the startrek one
		values[0] = new KVPair(ANSI_BLUE + System.getProperty("user.name") + ANSI_RESET + "@" + ANSI_BLUE,
				"lubuntu-PC" + ANSI_RESET);// recreates the first outputline of neofetch
		values[1]  = new KVPair("", separator);//
		values[2]  = new KVPair("OS:             ", "Debian");// change to the distribution name, I tried doing this with
				   										// code but it didnt work
		values[3]  = new KVPair("home directory: ", System.getProperty("user.home"));// your username
		values[4]  = new KVPair("username:       ", System.getProperty("user.name"));// $HOME
		values[5]  = new KVPair("Kernel:         ", System.getProperty("os.version"));// returns the version of your
				   																// kernel
		values[6]  = new KVPair("User shell:     ", System.getenv("SHELL"));
		values[7]  = new KVPair("Window manager: ", System.getenv("XDG_CURRENT_DESKTOP"));// should be the windowmanager
				   																		// or desktop enviroment you
				   																		// are using
		values[8]  = new KVPair("Editor:         ", System.getenv("EDITOR"));// this should be either nvim or emacs...
		values[9]  = new KVPair("colourSceme:    ", colours);
		values[10] = new KVPair("                ", colours);
		values[13] = new KVPair(":", "");
		values[13] = new KVPair(":", "");
		values[13] = new KVPair(":", "");// add your own values here and change the [index] to be the next one, and dont
										// forget to update the forloop
		int start = 10;// set to the amount of values you have set +1
		for (int i = start; i < values.length; i++) {
			values[i] = new KVPair("", "");// this sets upp empty lines
		}
	}
}
